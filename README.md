# Задания

- [IEEE 754 - стандарт двоичной арифметики с плавающей точкой](http://www.softelectro.ru/ieee754.html)
- [Tools & Thoughts. IEEE-754 Floating Point Converter](https://www.h-schmidt.net/FloatConverter/IEEE754.html)
- [Double (IEEE754 Double precision 64-bit)](https://www.binaryconvert.com/convert_double.html)
- [Задание 1](task-1.md) Номер задания согласно [списку](https://docs.google.com/spreadsheets/d/1MAp3meGfvVCROYvMo_6GvWg6vNYRpur4_GLqEC3joWU/edit).
- [Задание 2](task-2.md) Номер задания согласно [списку](https://docs.google.com/spreadsheets/d/1MAp3meGfvVCROYvMo_6GvWg6vNYRpur4_GLqEC3joWU/edit).


# Домашнее задание

- [Лабораторная работа № 2. Тема: «Представление информации в ПЭВМ типа IBM PC/AT»](/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D0%B8%D1%87%D0%BA%D0%B0_%D0%A1%D0%B8__%D0%9A%D1%80%D0%B0%D0%B2%D1%87%D1%83%D0%BA_.pdf)        
